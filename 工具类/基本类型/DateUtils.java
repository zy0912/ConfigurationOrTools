package com.example.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * <h3>日期处理工具类</h3>
 * <p></p>
 *
 * @author : zhengyue
 * @date : 2020-05-20 08:06
 **/
public class DateUtils {
    /**
     * 获取指定时间段内所有日期
     * @param dBegin
     * @param dEnd
     * @return
     */
    public static List<Date> getDates(Date dBegin, Date dEnd) {
        List<Date> lDate = new ArrayList<Date>();
        Calendar calBegin = Calendar.getInstance();
        Calendar calEnd = Calendar.getInstance();
        //给定起始日期和终止日期
        calBegin.setTime(dBegin);
        calEnd.setTime(dEnd);
        calBegin.add(Calendar.DAY_OF_MONTH, -1);
        while (dEnd.after(calBegin.getTime())) {
            // 根据日历的规则，为给定的日历字段添加或减去指定的时间量
            calBegin.add(Calendar.DAY_OF_MONTH, 1);
            lDate.add(calBegin.getTime());
        }
        return lDate;
    }
}